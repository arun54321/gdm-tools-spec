

Name:    gdm-tools
Version: 1.1
Release: 2%{?dist}
Summary: Set of tools for Gnome's Display/Login Manager (GDM).

License: GPLv3
URL:     https://github.com/realmazharhussain/gdm-tools
Source: https://github.com/realmazharhussain/gdm-tools/archive/refs/tags/v%{version}.tar.gz



BuildRequires: glib2-devel


Requires: dbus

%description
This is a set of tools for Gnome's Display/Login Manager (GDM).
Currently, it includes only 'set-gdm-theme' tool and 'gnomeconf2gdm'.



%prep
%setup 


%install
mkdir -p $RPM_BUILD_ROOT/usr/bin
mkdir -p $RPM_BUILD_ROOT/etc/gdm-tools
mkdir -p $RPM_BUILD_ROOT/usr/share//man/man1
cp bin/gnomeconf2gdm $RPM_BUILD_ROOT/usr/bin/
cp bin/set-gdm-theme $RPM_BUILD_ROOT/usr/bin/
cp config/set-gdm-theme.conf $RPM_BUILD_ROOT/etc/gdm-tools/
cp config/custom.css $RPM_BUILD_ROOT/etc/gdm-tools/
cp man1/set-gdm-theme.1 $RPM_BUILD_ROOT/usr/share/man/man1/

%files 
%{_bindir}/gnomeconf2gdm
%{_bindir}/set-gdm-theme

%dir %{_sysconfdir}/gdm-tools
%{_sysconfdir}/gdm-tools/set-gdm-theme.conf
%{_sysconfdir}/gdm-tools/custom.css
%{_mandir}/man1/set-gdm-theme.1.gz




%changelog

